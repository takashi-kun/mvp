namespace Installers
{
    using BlueprintFlow.ControlFlow;
    using Core.LocalDataServices;
    using Core.ScreenFlow.Installer;
    using Core.Utilities.ApplicationServices;
    using Utils;
    using Zenject;

    public class GameProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            UnityEngine.Debug.Log("Project installer");
            SignalBusInstaller.Install(this.Container);
            
            //Services
            this.Container.Bind<ILocalDataServices>().To<LocalDataServices>().AsSingle();
            
            //Generate fps
            this.Container.Bind<Fps>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();

            //Installer
            BlueprintServicesInstaller.Install(this.Container);
            ScreenFlowInstaller.Install(this.Container);
            ApplicationEventInstaller.Install(this.Container);
            
            //Models
        }
    }
}