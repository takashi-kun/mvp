namespace BlueprintFlow.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using BlueprintFlow.Reader.TypeConversionExtend;
    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;
    using MemberTypes = System.Reflection.MemberTypes;

    public static class CsvHelperExtensions
    {
        /// <summary>
        /// Register extended type converter for CSVHelper based on the data types contained in the database class 
        /// </summary>
        /// <param name="typeConverterCache"></param>
        /// <typeparam name="T">Type of the database class</typeparam>
        public static void RegisterTypeConverter<T>(this TypeConverterCache typeConverterCache) { RegisterTypeConverter(typeConverterCache, typeof(T)); }

        /// <summary>
        /// Register extended type converter for CSVHelper based on the data types contained in the database class 
        /// </summary>
        /// <param name="typeConverterCache"></param>
        /// <param name="databaseType">Type of the database class</param>
        public static void RegisterTypeConverter(this TypeConverterCache typeConverterCache, Type databaseType)
        {
            foreach (var property in databaseType.GetProperties())
            {
                if (property.PropertyType.IsGenericType)
                {
                    var genericTypeDefinition = property.PropertyType.GetGenericTypeDefinition();
                    if (genericTypeDefinition == typeof(List<>))
                    {
                        typeConverterCache.AddConverter(property.PropertyType, new ListGenericConverter());
                    }
                    else if (genericTypeDefinition == typeof(Dictionary<,>))
                    {
                        typeConverterCache.AddConverter(property.PropertyType, new DictionaryGenericConverter());
                    }
                }
                else
                {
                    if (property.PropertyType.IsArray)
                    {
                        typeConverterCache.AddConverter(property.PropertyType, new ArrayGenericConverter());
                    }
                }
            }
        }

        /// <summary>
        /// Utility for setting the member value for a specified object. Including fields and properties.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="target">The object whose member value will be set.</param>
        /// <param name="value">The new member value.</param>
        public static void SetMemberValue(this MemberInfo member, object target, object value)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    ((FieldInfo)member).SetValue(target, value);
                    break;
                case MemberTypes.Property:
                    ((PropertyInfo)member).SetValue(target, value, null);
                    break;
                default:
                    throw new ArgumentException("MemberInfo must be if type FieldInfo or PropertyInfo", "member");
            }
        }
        /// <summary>
        /// Utility to get all member infos from a class map
        /// </summary>
        public static Dictionary<string, MemberMapData> GetAllMemberInfos(this ClassMap classMap)
        {
            var results = new Dictionary<string, MemberMapData>();
            foreach (var memberMap in classMap.MemberMaps)
            {
                if (!memberMap.Data.Ignore)
                    results.Add(memberMap.Data.Member.Name, memberMap.Data);
            }

            // foreach (var referenceMap in classMap.ReferenceMaps)
            // {
            //     results.Add(referenceMap.Data.Member.Name, referenceMap.Data);
            // }

            return results;
        }
    }
}