namespace BlueprintFlow.Utilities
{
    using System;
    using System.IO;
    using System.Text;
    using UnityEngine;

    public class MD5
    {
        public static string GetMD5HashFromFile(string fileName)
        {
            try
            {
                using (var md5 = System.Security.Cryptography.MD5.Create())
                {
                    using (var stream = File.OpenRead(fileName))
                    {
                        var hash = md5.ComputeHash(stream);

                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < hash.Length; i++)
                        {
                            sb.Append(hash[i].ToString("X2"));
                        }

                        return sb.ToString().ToLower();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            return string.Empty;
        }
    }
}