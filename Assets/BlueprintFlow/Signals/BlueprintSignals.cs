namespace BlueprintFlow.Signals
{
    public class LoadBlueprintDataSignal
    {
        public string Url;
        public string Hash;
    }
    
    public class LoadBlueprintDataProgressSignal
    {
        public float percent;
    }

    public class LoadBlueprintDataSucceededSignal
    {

    }
}