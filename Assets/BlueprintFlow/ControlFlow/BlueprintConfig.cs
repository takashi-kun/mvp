namespace BlueprintFlow.ControlFlow
{
    using UnityEngine;

    public class BlueprintConfig
    {
        public static string CurrentBlueprintVersion = Application.version;

        public static readonly string PersistentDataPath         = Application.persistentDataPath;
        public const           string BlueprintZipFilename       = @"Blueprint_v{0}.zip";
        public static readonly string BlueprintZipFilepathFormat = $"{PersistentDataPath}/{BlueprintZipFilename}";

        public static string BlueprintZipFilepath
#if TEST_BLUEPRINT
            => "Assets/Resources/Blueprints/Blueprints_v1.0.zip";
#else
            => string.Format(BlueprintZipFilepathFormat, CurrentBlueprintVersion);
#endif

        public const string ResourceBlueprintPath = @"Blueprints/";
        public const string BlueprintFileType     = @".csv";
    }
}