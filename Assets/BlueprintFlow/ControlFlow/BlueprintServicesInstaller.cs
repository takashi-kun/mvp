namespace BlueprintFlow.ControlFlow
{
    using BlueprintFlow.Reader;
    using BlueprintFlow.Signals;
    using Utils;
    using Zenject;

    /// <summary>
    /// Binding all services of the blueprint control flow here
    /// </summary>
    public class BlueprintServicesInstaller : Installer<BlueprintServicesInstaller>
    {
        public override void InstallBindings()
        {
            this.Container.Bind<BlueprintDownloader>().WhenInjectedInto<BlueprintReaderManager>();
            this.Container.BindInterfacesAndSelfTo<BlueprintReaderManager>().AsSingle().NonLazy();
            
            this.Container.BindAllTypeDriveFrom<IGenericBlueprint>();

            this.Container.DeclareSignal<LoadBlueprintDataSignal>();
            this.Container.DeclareSignal<LoadBlueprintDataProgressSignal>();
            this.Container.DeclareSignal<LoadBlueprintDataSucceededSignal>();
        }
    }
}