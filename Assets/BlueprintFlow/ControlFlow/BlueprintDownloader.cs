namespace BlueprintFlow.ControlFlow
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using BlueprintFlow.Signals;
    using UnityEngine;
    using Zenject;

    public class BlueprintDownloader
    {
        private readonly SignalBus _signalBus;
        public BlueprintDownloader(SignalBus signalBus) { this._signalBus = signalBus; }

        public Task DownloadBlueprintAsync(string url)
        {
            try
            {
                using var client         = new WebClient();
                var       progressSignal = new LoadBlueprintDataProgressSignal();
                client.DownloadProgressChanged += (sender, e) =>
                {
                    progressSignal.percent = e.ProgressPercentage * 0.01f;
                    this._signalBus.Fire<LoadBlueprintDataProgressSignal>(progressSignal);
                };

                var task = client.DownloadFileTaskAsync(new Uri(url), BlueprintConfig.BlueprintZipFilepath);
                return task;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                throw;
            }
        }
    }
}