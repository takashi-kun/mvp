namespace BlueprintFlow.ControlFlow
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using BlueprintFlow.Reader;
    using BlueprintFlow.Signals;
    using BlueprintFlow.Utilities;
    using Core.LocalDataServices;
    using Core.Utilities.Extension;
    using Cysharp.Threading.Tasks;
    using Models;
    using UnityEngine;
    using Utils;
    using Zenject;

    /// <summary>
    /// The main manager for reading blueprints pipeline, triggered by <see cref="LoadBlueprintDataSignal"/>
    /// </summary>
    public class BlueprintReaderManager : IInitializable, IDisposable
    {
        private readonly SignalBus                  _signalBus;
        private readonly DiContainer                _diContainer;
        private readonly BlueprintDownloader        _downloader;
        private readonly ILocalDataServices         _localDataServices;
        private          Dictionary<string, string> _listRawBlueprints;

        public BlueprintReaderManager(SignalBus signalBus, DiContainer diContainer, BlueprintDownloader downloader, ILocalDataServices localDataServices)
        {
            this._signalBus         = signalBus;
            this._diContainer       = diContainer;
            this._downloader        = downloader;
            this._localDataServices = localDataServices;
            this._listRawBlueprints = new Dictionary<string, string>();
        }

        public void Initialize() { this._signalBus.Subscribe<LoadBlueprintDataSignal>(this.OnLoadBlueprint); }

        public void Dispose() { this._signalBus.Unsubscribe<LoadBlueprintDataSignal>(this.OnLoadBlueprint); }

        private async void OnLoadBlueprint(LoadBlueprintDataSignal signal)
        {
            Debug.Log($"Load blueprint version: {BlueprintConfig.CurrentBlueprintVersion}");
            if (!this.IsLoadLocalBlueprint(signal))
            {
                Debug.Log($"Download blueprint from {signal.Url}");
                await this._downloader.DownloadBlueprintAsync(signal.Url);

                var blueprintLocalData = this._localDataServices.Load<BlueprintLocalData>();
                blueprintLocalData.BlueprintDownloadUrl = signal.Url;
                this._localDataServices.Save(blueprintLocalData, true);
            }

            //Unzip
            this._listRawBlueprints = await UniTask.RunOnThreadPool(this.UnzipBlueprint);
            
            //Load to instances
            await this.ReadAllBlueprints();
            Debug.Log("[BlueprintReader] All blueprints are loaded");
            
            this._signalBus.Fire<LoadBlueprintDataSucceededSignal>();
        }

        private bool IsLoadLocalBlueprint(LoadBlueprintDataSignal signal)
        {
#if TEST_BLUEPRINT
            return true;
#else
            return this._localDataServices.Load<BlueprintLocalData>().BlueprintDownloadUrl == signal.Url
                   && File.Exists(BlueprintConfig.BlueprintZipFilepath)
                   && MD5.GetMD5HashFromFile(BlueprintConfig.BlueprintZipFilepath) == signal.Hash;
#endif
        }

        private async UniTask<Dictionary<string, string>> UnzipBlueprint()
        {
            var result = new Dictionary<string, string>();
            using (var archive = ZipFile.OpenRead(BlueprintConfig.BlueprintZipFilepath))
            {
                foreach (var entry in archive.Entries)
                {
                    if (!entry.FullName.EndsWith(BlueprintConfig.BlueprintFileType, StringComparison.OrdinalIgnoreCase)) continue;
                    using var streamReader = new StreamReader(entry.Open());
                    result.Add(entry.Name, await streamReader.ReadToEndAsync());
                }
            }

            return result;
        }

        private UniTask ReadAllBlueprints()
        {
            if (!File.Exists(BlueprintConfig.BlueprintZipFilepath))
            {
                Debug.LogError($"[BlueprintReader] {BlueprintConfig.BlueprintZipFilepath} does not exist!!!");
                return UniTask.CompletedTask;
            }

            var listReadTask = new List<UniTask>();
            foreach (var blueprintType in ReflectionUtils.GetAllDerivedTypes<IGenericBlueprint>())
            {
                var blueprintInstance = (IGenericBlueprint)this._diContainer.Resolve(blueprintType);
                if (blueprintInstance != null)
                {
                    listReadTask.Add(UniTask.RunOnThreadPool(() => OpenReadBlueprint(blueprintInstance)));
                }
                else
                {
                    Debug.Log($"Can not resolve blueprint: {blueprintType.Name}");
                }
            }
            
            return UniTask.WhenAll(listReadTask);
        }

        private async UniTask OpenReadBlueprint(IGenericBlueprint blueprint)
        {
            var bpAttribute = blueprint.GetCustomAttribute<BlueprintReaderAttribute>();
            if (bpAttribute != null)
            {
                if (bpAttribute.Ignore) return;

                string rawCsv = string.Empty;
                if (!bpAttribute.IsLoadFromResource)
                {
                    if (!this._listRawBlueprints.TryGetValue(bpAttribute.DataPath + BlueprintConfig.BlueprintFileType, out rawCsv))
                    {
                        Debug.LogWarning($"[BlueprintReader] Blueprint: {bpAttribute.DataPath} does not exist at local folder, try to load from resource");
                        rawCsv = await this.LoadRawCsvFromResourceFolder(bpAttribute);
                    }
                }
                else
                {
                    rawCsv = await this.LoadRawCsvFromResourceFolder(bpAttribute);
                }

                if (!string.IsNullOrEmpty(rawCsv))
                {
                    await blueprint.DeserializeFromCsv(rawCsv);
                }
                else
                {
                    Debug.LogWarning($"[BlueprintReader] Unable to load {bpAttribute.DataPath} from {(bpAttribute.IsLoadFromResource ? "Resource Folder" : "Local Folder")}!!!");
                }
            }
            else
            {
                Debug.LogWarning($"[BlueprintReader] Class {blueprint} does not have BlueprintReaderAttribute yet");
            }
        }

        private async UniTask<string> LoadRawCsvFromResourceFolder(BlueprintReaderAttribute readerAttribute)
        {
            await UniTask.SwitchToMainThread();
            try
            {
                return ((TextAsset)await Resources.LoadAsync<TextAsset>(BlueprintConfig.ResourceBlueprintPath + readerAttribute.DataPath)).text;
            }
            catch (Exception e)
            {
                Debug.LogError($"Load {readerAttribute.DataPath} blueprint error");
                Debug.LogException(e);
                return null;
            }
        }
    }
}