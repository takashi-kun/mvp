namespace BlueprintFlow.Reader
{
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class BlueprintReaderAttribute : Attribute
    {
        public string DataPath           { get; }
        public bool   IsLoadFromResource { get; }
        public bool   Ignore             { get; }

        public BlueprintReaderAttribute(string dataPath, bool isLoadFromResource = false, bool ignore = false)
        {
            this.DataPath           = dataPath;
            this.IsLoadFromResource = isLoadFromResource;
            this.Ignore             = ignore;
        }
    }
}