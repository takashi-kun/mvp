namespace BlueprintFlow.Reader
{
    using System.Threading.Tasks;

    /// <summary>
    /// Interface of database class
    /// </summary>
    public interface IGenericBlueprint
    {
        /// <summary>
        /// Auto binding data from the Raw CSV file to properties of database
        /// </summary>
        /// <param name="rawCsv"></param>
        /// <returns></returns>
        public Task DeserializeFromCsv(string rawCsv);
    }
}