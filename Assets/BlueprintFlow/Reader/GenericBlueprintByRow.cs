namespace BlueprintFlow.Reader
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using BlueprintFlow.Extensions;
    using CsvHelper;
    using Core.Utilities.Extension;
    using CsvHelper.Configuration;
    using UnityEngine;

    /// <summary> Attribute used to mark the Header Key for GenericDatabaseByRow </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class CsvHeaderKeyAttribute : Attribute
    {
        public readonly string HeaderKey;

        public CsvHeaderKeyAttribute(string headerKey) { this.HeaderKey = headerKey; }
    }

    /// <summary>
    /// An abstraction class for databases with row-based header fields
    /// </summary>
    /// <typeparam name="T1">Type of header key</typeparam>
    /// <typeparam name="T2">Type of value</typeparam>
    public abstract class GenericBlueprintByRow<T1, T2> : Dictionary<T1, T2>, IGenericBlueprint
    {
        public virtual async Task DeserializeFromCsv(string rawCsv)
        {
            this.CleanUp();
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
               MissingFieldFound = args =>
               {
                   Debug.LogWarning($"Field with names ['{string.Join("', '", args.HeaderNames)}'] at index '{args.Index}' was not found. Raw record: {args.Context.Parser.RawRecord}");
               },
            };
            using (var csv = new CsvReader(new StringReader(rawCsv), CultureInfo.InvariantCulture))
            {
                var csvHeaderKeyAttribute = this.GetCustomAttribute<CsvHeaderKeyAttribute>();
                var headerKey = csvHeaderKeyAttribute != null
                    ? csvHeaderKeyAttribute.HeaderKey
                    : "ID";

                csv.Context.TypeConverterCache.RegisterTypeConverter<T2>();
                await csv.ReadAsync();
                csv.ReadHeader();

                List<ISubBlueprintCollection> listSubBlueprintCollections = null;
                while (await csv.ReadAsync())
                {
                    if (!string.IsNullOrEmpty(csv.GetField(headerKey)))
                    {
                        var recordValue = csv.GetRecord<T2>();
                        this.Add(csv.GetField<T1>(headerKey), recordValue);

                        listSubBlueprintCollections = null;
                        foreach (var property in typeof(T2).GetProperties())
                        {
                            if ((property.PropertyType.IsGenericType || property.PropertyType.BaseType is { IsGenericType: true }) && typeof(ISubBlueprintCollection).IsAssignableFrom(property.PropertyType))
                            {
                                listSubBlueprintCollections ??= new List<ISubBlueprintCollection>();

                                var subCollection = (ISubBlueprintCollection)Activator.CreateInstance(property.PropertyType);
                                subCollection.Add(csv);
                                property.SetValue(recordValue,subCollection);
                                listSubBlueprintCollections.Add(subCollection);
                            }
                        }

                    }
                    else
                    {
                        if (listSubBlueprintCollections != null)
                        {
                            foreach (var subCollection in listSubBlueprintCollections)
                            {
                                subCollection.Add(csv);
                            }
                        }
                    }
                }
            }
        }

        public virtual T2 GetDataById(T1 id) { return this.TryGetValue(id, out var result) ? result : default(T2); }

        public virtual void CleanUp() { this.Clear(); }
    }

    public interface ISubBlueprintCollection
    {
        void Add(CsvReader inputCsv);
    } 
    
    public class SubBlueprintByRow<T> : List<T>, ISubBlueprintCollection
    {
        public void Add(CsvReader inputCsv)
        {
            this.Add(inputCsv.GetRecord<T>());
        }
    }

    public class SubBlueprintByRow<T1, T2> : Dictionary<T1, T2>, ISubBlueprintCollection
    {
        private string headerKey;
        public SubBlueprintByRow()
        {
            var csvHeaderKeyAttribute = this.GetCustomAttribute<CsvHeaderKeyAttribute>();
            this.headerKey = csvHeaderKeyAttribute != null
                ? csvHeaderKeyAttribute.HeaderKey
                : "SubID";
        }

        public void Add(CsvReader inputCsv)
        {
            if (!string.IsNullOrEmpty(inputCsv.GetField(this.headerKey)))
            {
                this.Add(inputCsv.GetField<T1>(this.headerKey), inputCsv.GetRecord<T2>());
            }
        }
    }
}