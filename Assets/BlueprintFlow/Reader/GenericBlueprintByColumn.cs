namespace BlueprintFlow.Reader
{
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using BlueprintFlow.Extensions;
    using CsvHelper;

    /// <summary>
    /// An abstract class for database with column-based header fields
    /// </summary>
    public abstract class GenericBlueprintByColumn : IGenericBlueprint
    {
        public async Task DeserializeFromCsv(string rawCsv)
        {
            using (var csv = new CsvReader(new StringReader(rawCsv), CultureInfo.InvariantCulture))
            {
                csv.Context.TypeConverterCache.RegisterTypeConverter(this.GetType());
                var classMap   = csv.Context.AutoMap(this.GetType());
                var allMembers = classMap.GetAllMemberInfos();

                await csv.ReadAsync();
                csv.ReadHeader();
                while (await csv.ReadAsync())
                {
                    if (allMembers.TryGetValue(csv.GetField(0), out var memberInfo))
                    {
                        memberInfo.Index                = 1;
                        memberInfo.TypeConverter        = csv.Context.TypeConverterCache.GetConverter(memberInfo.Type);
                        memberInfo.TypeConverterOptions = csv.Context.TypeConverterOptionsCache.GetOptions(memberInfo.Type);

                        memberInfo.Member.SetMemberValue(this, memberInfo.TypeConverter.ConvertFromString(csv.GetField(1), csv, memberInfo));
                    }
                }
            }
        }
    }
}