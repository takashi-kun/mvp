namespace BlueprintFlow.Reader.TypeConversionExtend
{
    using System;
    using CsvHelper;
    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;

    public class ArrayGenericConverter : DefaultTypeConverter
    {
        private readonly char delimiter;
        public ArrayGenericConverter(char delimiter = ',') { this.delimiter = delimiter; }

        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var stringData = text.Split(this.delimiter);

                var arraySize = stringData.Length;
                var array     = (Array)ObjectResolver.Current.Resolve(memberMapData.Member.MemberType(), arraySize);

                var converter = row.Context.TypeConverterCache.GetConverter(memberMapData.Member.MemberType().GetElementType());

                for (int i = 0; i < arraySize; i++)
                {
                    array.SetValue(converter.ConvertFromString(stringData[i], row, memberMapData), i);
                }

                return array;
            }

            return null;
        }
    }
}