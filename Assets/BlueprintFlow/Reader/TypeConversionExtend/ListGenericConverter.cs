namespace BlueprintFlow.Reader.TypeConversionExtend
{
    using System.Collections;
    using System.Collections.Generic;
    using CsvHelper;
    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;

    public class ListGenericConverter : DefaultTypeConverter
    {
        private readonly char delimiter;
        public ListGenericConverter(char delimiter = ',') { this.delimiter = delimiter; }

        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var type       = memberMapData.Member.MemberType().GetGenericArguments()[0];
                var list       = (IList)ObjectResolver.Current.Resolve(typeof(List<>).MakeGenericType(type));
                var converter  = row.Context.TypeConverterCache.GetConverter(type);
                var stringData = text.Split(this.delimiter);

                foreach (var s in stringData)
                {
                    list.Add(converter.ConvertFromString(s, row, memberMapData));
                }

                return list;
            }

            return null;
        }
    }
}