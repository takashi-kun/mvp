namespace BlueprintFlow.Reader.TypeConversionExtend
{
    using System;
    using CsvHelper;
    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;

    public static class NumericType
    {
        public const string Dollars = "{0:0.00}";
        public const string Hours   = "{0:0.000}";
        public const string PayRate = "{0:0.0000}";
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DecimalFormatAttribute : System.Attribute
    {
        public string Format { get; } = "{0}";

        public DecimalFormatAttribute(string format) => this.Format = format;
    }

    public class DecimalConverter : DefaultTypeConverter
    {
        public string Format { get; } = "{0}";
		
        public DecimalConverter(string format) => this.Format = format;

        public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            if (value is decimal d)
                return (d == 0) ? string.Empty : string.Format(this.Format, d);

            return base.ConvertToString(value, row, memberMapData);
        }
    }
}