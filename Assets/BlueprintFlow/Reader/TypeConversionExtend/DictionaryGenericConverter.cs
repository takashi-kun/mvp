namespace BlueprintFlow.Reader.TypeConversionExtend
{
    using System.Collections;
    using System.Collections.Generic;
    using CsvHelper;
    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;

    public class DictionaryGenericConverter: DefaultTypeConverter
    {
        private readonly char delimiterItem;
        private readonly char delimiterPair;
        public DictionaryGenericConverter(char delimiterItem = ',', char delimiterPair = ':')
        {
            this.delimiterItem = delimiterItem;
            this.delimiterPair = delimiterPair;
        }

        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var keyType        = memberMapData.Member.MemberType().GetGenericArguments()[0];
                var valueType      = memberMapData.Member.MemberType().GetGenericArguments()[1];
                var dictionaryType = typeof(Dictionary<,>).MakeGenericType(keyType, valueType);
                
                var keyConverter   = row.Context.TypeConverterCache.GetConverter(keyType);
                var valueConverter = row.Context.TypeConverterCache.GetConverter(valueType);
                var itemsRawData   = text.Split(this.delimiterItem);
                var dictionary     = (IDictionary)ObjectResolver.Current.Resolve(dictionaryType);
                foreach (var itemRawData in itemsRawData)
                {
                    var itemData = itemRawData.Split(this.delimiterPair);
                    dictionary.Add(keyConverter.ConvertFromString(itemData[0], row, memberMapData),valueConverter.ConvertFromString(itemData[1], row, memberMapData));
                }

                return dictionary;
            }

            return null;
        }
    }
}