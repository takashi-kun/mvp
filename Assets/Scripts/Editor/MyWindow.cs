using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MVP.Testing;
using UnityEditor;
using UnityEngine;
using Utils;

public class MyWindow : EditorWindow
{
    [MenuItem("My Window/Open")]
    internal static void GetWindow()
    {
        GetWindow<MyWindow>();
    }

    private List<Type> _inheritances;
    private int               _selectionGrid;

    private void OnEnable()
    {
        this._inheritances = ReflectionUtils.GetAllDerivedTypes<IInterface>().ToList();
    }


    private void OnGUI()
    {
        EditorGUILayout.LabelField("This is my window");

        foreach (var inheritance in this._inheritances)
        {
            EditorGUILayout.LabelField(inheritance.Name);
        }

        string[] arr = this._inheritances.Select(_ => _.Name).ToArray();
        
        _selectionGrid = GUILayout.SelectionGrid(this._selectionGrid, arr, arr.Length, EditorStyles.radioButton);

        if (GUILayout.Button("Button"))
        {
            Debug.Log($"You are select: {this._inheritances[this._selectionGrid].Name}");
            Type type = this._inheritances[this._selectionGrid];
            if (type == typeof(InheritA))
            {
                Debug.Log("AAAA");
            }
        }
    }
}
