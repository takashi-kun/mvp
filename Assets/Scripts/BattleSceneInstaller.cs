namespace MVP
{
    using Core.ScreenFlow.Installer;
    using UI;
    using Utils;

    public class BattleSceneInstaller : BaseSceneInstaller
    {
        public override void InstallBindings()
        {
            base.InstallBindings();
            this.Container.InitScreenManually<UIScreenBattlePresenter>();
        }
    }
}