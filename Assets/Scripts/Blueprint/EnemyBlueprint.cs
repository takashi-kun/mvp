namespace Blueprint
{
    using BlueprintFlow.Reader;

    [CsvHeaderKey("ID")]
    [BlueprintReader("Enemy")]
    public class EnemyBlueprint : GenericBlueprintByRow<int, EnemyRecord>
    {
        
    }

    public class EnemyRecord
    {
        public int                      ID                       { get; set; }
        public string                   Name                     { get; set; }
        public string                   AssetPath                { get; set; }
        public EnemyType                EnemyType                { get; set; }
        public EnemyLevelDataCollection EnemyLevelDataCollection { get; set; }
    }

    [CsvHeaderKey("Level")]
    public class EnemyLevelDataCollection : SubBlueprintByRow<int, EnemyLevelDataRecord> { }

    public class EnemyLevelDataRecord
    {
        public int Level  { get; set; }
        public int Damage { get; set; }
    }

    public enum EnemyType
    {
        Human,
        Mechanic
    }
}