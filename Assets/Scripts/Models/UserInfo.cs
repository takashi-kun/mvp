namespace MVP.Models
{
    public class UserInfo
    {
        public string Name     { get; set; }
        public bool   IsFemale { get; set; }
        public int    Age      { get; set; }
        public string Address  { get; set; }
    }
}