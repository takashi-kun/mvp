namespace UI
{
    using Core.ScreenFlow.BaseScreen.Presenter;
    using Core.ScreenFlow.BaseScreen.View;
    using Core.ScreenFlow.Managers;
    using MVP.Models;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    [PopupInfo("UIPopupUserInfo", true)]
    public class UIPopupUserInfoPresenter : BasePopupPresenter<UIPopupUserInfoView, UserInfo>
    {
        private int _sampleCounter;
        
        private readonly IScreenManager _screenManager;
        
        public UIPopupUserInfoPresenter(SignalBus signalBus, IScreenManager screenManager) : base(signalBus)
        {
            this._screenManager = screenManager;
        }
        
        public override void BindData(UserInfo popupModel)
        {
            this._sampleCounter++;
            this.View.TxtCounter.text = $"Counter: {this._sampleCounter}";
            
            this.View.TxtName.text    = $"Name: {popupModel.Name}";
            this.View.TxtSex.text     = $"Sex: {(popupModel.IsFemale ? "Female" : "Male")}";
            this.View.TxtAge.text     = $"Age: {popupModel.Age.ToString()}";
            this.View.TxtAddress.text = $"Address: {popupModel.Address}";
            
            this.View.BtnInfo.onClick.AddListener(this.OnOpenPopupInfo);
        }

        public override bool EscapeAction()
        {
            return this._sampleCounter >= 10;
        }

        private void OnOpenPopupInfo()
        {
            this._screenManager.OpenScreen<UIPopupInfoPresenter, string>("User Info");
        }

        public override void Dispose()
        {
            this.View.BtnInfo.onClick.RemoveAllListeners();
            base.Dispose();
        }
    }

    public class UIPopupUserInfoView : BaseView
    {
        [SerializeField] private TextMeshProUGUI _txtCounter;
        [SerializeField] private TextMeshProUGUI _txtName;
        [SerializeField] private TextMeshProUGUI _txtSex;
        [SerializeField] private TextMeshProUGUI _txtAge;
        [SerializeField] private TextMeshProUGUI _txtAddress;
        
        [SerializeField] private Button _btnInfo;

        public TextMeshProUGUI TxtCounter    => this._txtCounter;
        public TextMeshProUGUI TxtName    => this._txtName;
        public TextMeshProUGUI TxtSex     => this._txtSex;
        public TextMeshProUGUI TxtAge     => this._txtAge;
        public TextMeshProUGUI TxtAddress => this._txtAddress;
        
        public Button BtnInfo => this._btnInfo;
    }
}