namespace UI
{
    using System;
    using Core.ScreenFlow.BaseScreen.Presenter;
    using Core.ScreenFlow.BaseScreen.View;
    using TMPro;
    using UnityEngine;
    using Zenject;

    [PopupInfo("UIPopupInfo", true)]
    public class UIPopupInfoPresenter : BasePopupPresenter<UIPopupInfoView, string>
    {
        public UIPopupInfoPresenter(SignalBus signalBus) : base(signalBus)
        {
        }

        protected override void OnViewReady()
        {
            Debug.Log("Bind Static Data here");
            this.View.TxtScreenInfo.text = "View Ready";
            
            base.OnViewReady();
        }

        public override void BindData(string popupModel)
        {
            this.View.TxtScreenInfo.text = popupModel;
        }
    }

    public class UIPopupInfoView : BaseView
    {
        [SerializeField] private TextMeshProUGUI _txtScreenInfo;

        public TextMeshProUGUI TxtScreenInfo => this._txtScreenInfo;
    }
}