namespace UI
{
    using Core.ScreenFlow.BaseScreen.Presenter;
    using Core.ScreenFlow.BaseScreen.View;
    using Core.ScreenFlow.Managers;
    using MVP.Models;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    [ScreenInfo(@"UIScreenBattle")]
    public class UIScreenBattlePresenter : BaseScreenPresenter<UIScreenBattleView>
    {
        private IScreenManager _screenManager;
        public UIScreenBattlePresenter(SignalBus signalBus, IScreenManager screenManager) : base(signalBus)
        {
            this._screenManager = screenManager;
        }
        
        protected override void OnViewReady()
        {
            base.OnViewReady();
            this.OpenView();
        }
        
        public override void BindData()
        {
            this.View.TxtScreenInfo.text = "The Screen Battle View";
            this.View.BtnInfo.onClick.AddListener(this.OnOpenPopupInfo);
            this.View.BtnUserInfo.onClick.AddListener(this.OnOpenUserPopupInfo);
        }

        private void OnOpenPopupInfo()
        {
            this._screenManager.OpenScreen<UIPopupInfoPresenter, string>("This is info popup");
        }
        
        private void OnOpenUserPopupInfo()
        {
            this._screenManager.OpenScreen<UIPopupUserInfoPresenter, UserInfo>(new UserInfo()
            {
                Name = "Daphne",
                IsFemale = true,
                Age = 20,
                Address = "Honolulu, Hawaii"
            });
        }

        public override void Dispose()
        {
            this.View.BtnInfo.onClick.RemoveAllListeners();
            this.View.BtnUserInfo.onClick.RemoveAllListeners();
            base.Dispose();
        }
    }

    public class UIScreenBattleView : BaseView
    {
        [SerializeField] private TextMeshProUGUI _txtScreenInfo;
        [SerializeField] private Button          _btnInfo;
        [SerializeField] private Button          _btnUserInfo;

        public TextMeshProUGUI TxtScreenInfo => this._txtScreenInfo;
        public Button          BtnInfo       => this._btnInfo;
        public Button          BtnUserInfo   => this._btnUserInfo;
    }
}