namespace MVP.Testing
{
    using System;
    using Blueprint;
    using BlueprintFlow.Signals;
    using UnityEngine;
    using Utils;
    using Zenject;

    public class MonoTest : MonoBehaviour
    {
        public string blueprintUrl;
        public string blueprintHash;
        
        private SignalBus      _signal;
        private EnemyBlueprint _blueprint;
        [Inject]
        void Init(SignalBus signalBus, EnemyBlueprint blueprint)
        {
            this._signal    = signalBus;
            this._blueprint = blueprint;
        }
        
        private void Start()
        {
            this.Listing();
            this._signal.Fire(new LoadBlueprintDataSignal()
            {
                Url  = this.blueprintUrl,
                Hash = this.blueprintHash
            });
        }

        private void Listing()
        {
            foreach (var type in ReflectionUtils.GetAllDerivedTypes<IInterface>())
            {
                Debug.Log(type.Name);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log(this._blueprint[0].Name);
                foreach (var level in this._blueprint[0].EnemyLevelDataCollection.Values)
                {
                    Debug.Log($"Level: {level.Level}, Damage: {level.Damage}");
                }
            }
            
            if (Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log(this._blueprint[1].Name);
                foreach (var level in this._blueprint[1].EnemyLevelDataCollection.Values)
                {
                    Debug.Log($"Level: {level.Level}, Damage: {level.Damage}");
                }
            }
        }
    }
}