namespace MVP.Testing
{
    using UnityEngine;

    public interface IInterface
    {
        void DoSomething();
    }

    public class InheritA : IInterface
    {
        public void DoSomething()
        {
            Debug.Log("This is A");
        }
    }
    
    public class InheritB : IInterface
    {
        public void DoSomething()
        {
            Debug.Log("This is B");
        }
    }
    
    public class InheritC : IInterface
    {
        public void DoSomething()
        {
            Debug.Log("This is C");
        }
    }
    
    public class InheritD : IInterface
    {
        public void DoSomething()
        {
            Debug.Log("This is D");
        }
    }
    
    public class InheritE : IInterface
    {
        public void DoSomething()
        {
            Debug.Log("This is E");
        }
    }
    
    public abstract class InheritF : IInterface
    {
        public void DoSomething()
        {
            Debug.Log("This is F");
        }
    }

    public class InheritFromF : InheritF
    {
        
    }
}