namespace Core.AssetLibrary
{
    using Cysharp.Threading.Tasks;
    using UnityEngine;

    public static class GameAssets
    {
        public static UniTask<Object> LoadAssetAsync(string key)
        {
            var resourceRequest = Resources.LoadAsync<Object>(key);
            return resourceRequest.ToUniTask();
        }
    }
}