namespace Core.Utilities.Extension
{
    using System.Threading.Tasks;
    using Core.AssetLibrary;
    using Core.MVP;
    using Core.ScreenFlow.BaseScreen.Presenter;
    using Core.ScreenFlow.BaseScreen.View;
    using Newtonsoft.Json;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public static class UIExtension
    {
        //Remove all Button Listener On View
        public static void OnRemoveButtonListener(this MonoBehaviour view)
        {
            var buttons = view.GetComponentsInChildren<Button>();
            foreach (var b in buttons)
            {
                b.onClick.RemoveAllListeners();
            }
        }

        //check Object trigger With other object
        public static bool CheckObjectOnBound(this BaseView view, Bounds bounds, Bounds g) { return bounds.Intersects(g); }

        //Create view
        public static async Task<T> CreateView<T>(this IUIPresenter iScreen,string nameObject, Transform parent) where T : IUIView
        {
            var viewObject = Object.Instantiate(await GameAssets.LoadAssetAsync(nameObject) as GameObject, parent).GetComponent<T>();
            return viewObject;
        }
        
        public static void InstantiateUIPresenter<TPresenter, TView, TModel>(this IInstantiator instantiator, ref TPresenter presenter, TView view, TModel model ) where TPresenter : BaseUIItemPresenter<TView, TModel> where TView : IUIView
        {
            if (presenter == null)
            {
                presenter = instantiator.Instantiate<TPresenter>();
                presenter.SetView(view);
            }
            presenter.BindData(model);
        }

        //FillChild Width with parent Width
        public static void FillChildWidthWithParentWidth(this IUIPresenter presenter, RectTransform childRect, RectTransform parentRect)
        {
            var v = childRect.sizeDelta;
            v.x                 = parentRect.rect.width;
            childRect.sizeDelta = v;
        }

        public static string ToJsonString(this object o)
        {
            return JsonConvert.SerializeObject(o);
        }
    }
}