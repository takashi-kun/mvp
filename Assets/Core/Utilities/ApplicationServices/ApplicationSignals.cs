namespace Core.Utilities.ApplicationServices
{
    /// <summary>Model signal application event pause, focus...</summary>
    public class ApplicationPauseSignal
    {
        public bool PauseStatus;
    }
}