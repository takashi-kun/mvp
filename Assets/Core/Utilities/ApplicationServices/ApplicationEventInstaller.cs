namespace Core.Utilities.ApplicationServices
{
    using Zenject;

    public class ApplicationEventInstaller : Installer<ApplicationEventInstaller>
    {
        public override void InstallBindings()
        {
            this.Container.DeclareSignal<ApplicationPauseSignal>();
            this.Container.Bind<ApplicationEventHelper>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        }
    }
}