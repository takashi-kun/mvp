namespace Core.Utilities.ApplicationServices
{
    using Core.LocalDataServices;
    using UnityEngine;
    using Zenject;

    public class ApplicationEventHelper : MonoBehaviour
    {
        private SignalBus              _signalBus;
        private ILocalDataServices     _localDataServices;
        private ApplicationPauseSignal _pause;


        [Inject]
        public void Init(SignalBus signalBus, ILocalDataServices localDataServices)
        {
            this._signalBus        = signalBus;
            this._localDataServices = localDataServices;
            this._pause            = new ApplicationPauseSignal() { PauseStatus = false };
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            this._pause.PauseStatus = pauseStatus;
            this._signalBus.Fire(this._pause);
            
            // save local data to storage
            if (pauseStatus)
            {
                this._localDataServices.StoreAllToLocal();
            }
        }

        private void OnApplicationQuit() { this._localDataServices.StoreAllToLocal(); }
    }
}