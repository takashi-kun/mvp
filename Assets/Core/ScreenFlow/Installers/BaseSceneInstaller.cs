namespace Core.ScreenFlow.Installer
{
    using Core.ScreenFlow.Managers;
    using UnityEngine;
    using Zenject;

    public class BaseSceneInstaller : MonoInstaller
    {
        /// <summary>
        /// Instance of Root UI Canvas on Scene
        /// </summary>
        [SerializeField] private RootUICanvas _rootUICanvas;

        [Inject] private IScreenManager screenManager;
        
        public override void InstallBindings()
        {
            this.screenManager.CurrentRootScreen = this._rootUICanvas.RootUIShowTransform;
            this.screenManager.CurrentHiddenRoot = this._rootUICanvas.RootUIClosedTransform;
            this.screenManager.Instantiator      = this.Container;
        }
    }
}