namespace Core.ScreenFlow.Signals
{
    using Core.ScreenFlow.BaseScreen.Presenter;

    public class PopupShowedSignal
    {
        public IScreenPresenter ScreenPresenter;
    }
    
    public class PopupHiddenSignal
    {
        public IScreenPresenter ScreenPresenter;
    }
    
    public class PopupBlurBgShowedSignal
    {
    }
}