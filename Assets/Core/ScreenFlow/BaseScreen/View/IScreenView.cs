namespace Core.ScreenFlow.BaseScreen.View
{
    using System;
    using Core.MVP;
    using UnityEngine;

    /// <summary>
    /// The responsibilities of a View are:
    /// - Handle ref to elements needed to drawing (Textures, Fxs, etc)
    /// - Perform Animations
    /// - Receive User Input
    /// -...
    /// </summary>
    public interface IScreenView : IUIView
    {
        public RectTransform RectTransform { get; }
        public bool          IsReadyToUse  { get; }
        public void          Open();
        public void          Close();
        public void          Hide();

        public void DestroySelf();

        public event Action ViewDidClose;
        public event Action ViewDidOpen;
        public event Action ViewDidDestroy;
    }
}