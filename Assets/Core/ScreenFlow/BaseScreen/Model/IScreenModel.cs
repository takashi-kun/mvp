namespace Core.ScreenFlow.BaseScreen.Model
{
    using Core.MVP;

    /// <summary>
    /// The Traditional Model:
    /// - Holds no View data nor View State Data
    /// - Is accessed by the Presenter and other Models only
    /// - Will trigger events to notify external system of changes
    /// </summary>
    public interface IScreenModel : IUIModel
    {
        
    }
}