namespace Core.ScreenFlow.BaseScreen.Presenter
{
    using System;

    /// <summary>
    /// Attributes for store basic information of a screen
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ScreenInfoAttribute : Attribute
    {
        public string ScreenAssetPath { get; }
        public ScreenInfoAttribute(string screenAssetPath)
        {
            ScreenAssetPath = screenAssetPath;
        }
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class PopupInfoAttribute : ScreenInfoAttribute
    {
        public bool IsCloseWhenTapOutside { get; }

        public PopupInfoAttribute(string screenAssetPath, bool isCloseWhenTapOutside) : base(screenAssetPath)
        {
            IsCloseWhenTapOutside = isCloseWhenTapOutside;
        }
    }
}