namespace Core.ScreenFlow.BaseScreen.Presenter
{
    using System;
    using Core.AssetLibrary;
    using Core.MVP;
    using Cysharp.Threading.Tasks;
    using UnityEngine;
    using Zenject;
    using Object = UnityEngine.Object;

    /// <summary>
    /// Base UI presenter for item
    /// </summary>
    /// <typeparam name="TView">Type of view</typeparam>
    public abstract class BaseUIItemPresenter<TView> : IUIPresenter where TView : IUIView
    {
        protected         TView  View;
        protected virtual string PrefabPath { get; } = typeof(TView).Name;

        /// <summary>
        /// Set view automatically
        /// </summary>
        /// <param name="parent"></param>
        public async UniTask SetView(Transform parent)
        {
            this.View ??= Object.Instantiate(await GameAssets.LoadAssetAsync(this.PrefabPath) as GameObject, parent).GetComponent<TView>();
        }

        /// <summary>
        /// Set view manually
        /// </summary>
        /// <param name="viewInstance"></param>
        public void SetView(IUIView viewInstance) { this.View = (TView)viewInstance; }
    }

    public abstract class BaseUIItemPresenter<TView, TModel> : BaseUIItemPresenter<TView> where TView : IUIView
    {
        public abstract void BindData(TModel param);
    }

    public abstract class BaseUIItemPresenter<TView, TModel1, TModel2> : BaseUIItemPresenter<TView> where TView : IUIView
    {
        public abstract void BindData(TModel1 param1, TModel2 param2);
    }

    /// <summary>
    /// Base UI presenter for item that can poolable.
    /// For more information (example), see <see cref="Zenject.SpaceFighter.GameInstaller"/>
    /// </summary>
    /// <typeparam name="TView">Type of view</typeparam>
    public abstract class BaseUIItemPoolablePresenter<TView> : BaseUIItemPresenter<TView>, IPoolable<IMemoryPool>, IDisposable where TView : MonoBehaviour, IUIView
    {
        private IMemoryPool pool;

        public void SetActiveView(bool value)
        {
            if(this.View != null) this.View.gameObject.SetActive(value);
        }

        public void OnDespawned()
        {
            this.pool = null;
            this.SetActiveView(false);
        }

        public void OnSpawned(IMemoryPool pool)
        {
            this.pool = pool;
            this.SetActiveView(true);
        }

        public void Dispose() { this.pool.Despawn(this); }
    }

    public abstract class BaseUIItemPoolablePresenter<TView, TModel> : BaseUIItemPoolablePresenter<TView> where TView : MonoBehaviour, IUIView
    {
        public abstract void BindData(TModel param);
    }

    public abstract class BaseUIItemPoolablePresenter<TView, TModel1, TModel2> : BaseUIItemPoolablePresenter<TView> where TView : MonoBehaviour, IUIView
    {
        public abstract void BindData(TModel1 param1, TModel2 param2);
    }
}