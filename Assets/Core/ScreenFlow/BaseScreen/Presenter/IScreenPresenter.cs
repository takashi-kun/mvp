namespace Core.ScreenFlow.BaseScreen.Presenter
{
    using System;
    using Core.MVP;
    using UnityEngine;
    using Zenject;

    /// <summary>
    /// The Presenter is the link between the Model and the View. It holds th state of the View and updates it depending on that state and on external events:
    /// - Holds the application state needed for that View
    /// - Controls view flow
    /// - Show/Hide/Activate/DeActivate/Update the View of parts of the View depending on the state
    /// - Handles events either triggered by the player in the View (eg. the player touched the button)
    ///   or triggered by the Model (e.g. the player has gained XP and that triggered a Level Up event so the controller updates the level Number in the view)
    /// </summary>
    public interface IScreenPresenter : IUIPresenter, IInitializable, IDisposable
    {
        public bool         IsClosePrevious { get; }
        public ScreenStatus ScreenStatus    { get; }

        public void SetViewParent(Transform parent);
        
        public void BindData();

        public void OpenView();
        public void CloseView();
        public void HideView();
        public bool EscapeAction();
        public void DestroyView();

        /// <summary>
        /// Called when the screen is overlap by another screen 
        /// </summary>
        public void OnOverlap();

        public int ViewSiblingIndex { get; set; }
    }
    
    public interface IScreenPresenter<TModel> : IScreenPresenter
    {
        public void OpenView(TModel model);

    }

    public enum ScreenStatus
    {
        Opened,
        Closed,
        Hide,
        Destroyed,
    }
}