namespace Core.ScreenFlow.BaseScreen.Presenter
{
    using Core.ScreenFlow.BaseScreen.View;
    using Core.ScreenFlow.Signals;
    using Zenject;

    public abstract class BasePopupPresenter<TView> : BaseScreenPresenter<TView> where TView : IScreenView
    {
        public BasePopupPresenter(SignalBus signalBus) : base(signalBus) { }

        protected override void OnViewReady()
        {
            base.OnViewReady();
            this.View.ViewDidOpen  += this.OnViewDidOpen;
            this.View.ViewDidClose += OnViewDidClose;
        }
        
        private void OnViewDidOpen()  { this.SignalBus.Fire(new PopupShowedSignal() { ScreenPresenter = this }); }
        private void OnViewDidClose() { this.SignalBus.Fire(new PopupHiddenSignal() { ScreenPresenter = this }); }
        protected override void OnViewDestroyed()
        {
            base.OnViewDestroyed();
            this.SignalBus.Fire(new PopupHiddenSignal()
            {
                ScreenPresenter = this
            }); 
        }
    }

    public abstract class BasePopupPresenter<TView, TModel> : BasePopupPresenter<TView>, IScreenPresenter<TModel> where TView : IScreenView
    {
        protected TModel Model;
        
        protected BasePopupPresenter(SignalBus signalBus) : base(signalBus) { }

        public override void OpenView()
        {
            base.OpenView();
            if (this.Model != null)
            {
                this.BindData(this.Model);
            }
            else
            {
                UnityEngine.Debug.LogWarning($"{this.GetType().Name} don't have Model!!!");
            }
        }

        public void OpenView(TModel model)
        {
            if (model != null)
            {
                this.Model = model;
            }
            this.OpenView();
        }

        public override void HideView()
        {
            if (this.ScreenStatus == ScreenStatus.Hide) return;
            this.ScreenStatus = ScreenStatus.Hide;
            this.Dispose();
        }

        public sealed override void BindData() { }

        public abstract void BindData(TModel popupModel);
    }
}