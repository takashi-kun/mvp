namespace Core.ScreenFlow.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Core.ScreenFlow.BaseScreen.Presenter;
    using Core.ScreenFlow.Signals;
    using Cysharp.Threading.Tasks;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class PopupBackgroundManager : MonoBehaviour
    {
        [SerializeField] private Image  _imgBlur;
        [SerializeField] private Button _btnClose;

        private IScreenPresenter _currentPopup;
        private SignalBus        _signalBus;

        private readonly Dictionary<Type, PopupInfoAttribute> _popupInfoPool = new Dictionary<Type, PopupInfoAttribute>();

        [Inject]
        private void Init(SignalBus signalBus)
        {
            this._signalBus = signalBus;
            
            this._signalBus.Subscribe<PopupShowedSignal>(this.OnPopupShowed);
            this._signalBus.Subscribe<PopupHiddenSignal>(this.OnPopupHidden);

            this._btnClose.onClick.AddListener(this.OnCloseButton);
        }
        
        private void OnDestroy()
        {
            this._signalBus.Unsubscribe<PopupShowedSignal>(this.OnPopupShowed);
            this._signalBus.Unsubscribe<PopupHiddenSignal>(this.OnPopupHidden);
        }

        private void OnCloseButton()
        {
            if (this._currentPopup != null && this.GetPopupInfo(this._currentPopup).IsCloseWhenTapOutside)
            {
                if (this._currentPopup.EscapeAction())
                {
                    this._currentPopup.CloseView();
                }
            }
        }

        private void OnPopupHidden(PopupHiddenSignal signal)
        {
            if (this._currentPopup != null && this._currentPopup.ScreenStatus != ScreenStatus.Opened)
            {
                this.SetBlur(false);
                this._currentPopup          = null;
            }
        }

        private void OnPopupShowed(PopupShowedSignal signal)
        {
            this._currentPopup = signal.ScreenPresenter;
            
            this.SetBlur(true);
        }

        private async void SetBlur(bool enable)
        {
            await UniTask.NextFrame();
            
            this._imgBlur.enabled       = enable;
            this._imgBlur.raycastTarget = enable;

            if (enable)
            {
                int currentIndex = this._currentPopup.ViewSiblingIndex;
                
                var blurTrans    = this._imgBlur.rectTransform;
                if (blurTrans.GetSiblingIndex() > currentIndex) currentIndex++;
                
                blurTrans.SetSiblingIndex(currentIndex - 1);
                
                this._signalBus.Fire<PopupBlurBgShowedSignal>();
            }
        }
        
        private PopupInfoAttribute GetPopupInfo(IScreenPresenter popup)
        {
            var popupType = popup.GetType();
            if (!this._popupInfoPool.TryGetValue(popupType, out var result))
            {
                result = popupType.GetCustomAttribute<PopupInfoAttribute>();
                this._popupInfoPool.Add(popupType, result);
            }

            return result;
        }
    }
}