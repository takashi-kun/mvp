namespace Core.ScreenFlow.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.AssetLibrary;
    using Core.ScreenFlow.BaseScreen.Presenter;
    using Core.ScreenFlow.BaseScreen.View;
    using Core.ScreenFlow.Signals;
    using Core.Utilities.Extension;
    using Cysharp.Threading.Tasks;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using Zenject;

    public class ScreenManager : MonoBehaviour, IScreenManager, IDisposable
    {
        #region Properties

        /// <summary>
        /// List of active screens
        /// </summary>
        [SerializeField] private List<IScreenPresenter> _activeScreens;

        /// <summary>
        /// Current screen shown on top.
        /// </summary>
        [SerializeField] private IScreenPresenter _currentActiveScreen;

        [SerializeField] private IScreenPresenter _previousActiveScreen;

        private Dictionary<Type, IScreenPresenter> _screenPool;


        private SignalBus    _signalBus;
        private RootUICanvas _rootUICanvas;

        #endregion

        [Inject]
        public void Init(SignalBus signalBusParam)
        {
            this._signalBus   = signalBusParam;

            this._activeScreens = new List<IScreenPresenter>();
            this._screenPool    = new Dictionary<Type, IScreenPresenter>();

            this._signalBus.Subscribe<StartLoadingNewSceneSignal>(this.CleanUpAllScreen);
            this._signalBus.Subscribe<ScreenShowSignal>(this.OnShowScreen);
            this._signalBus.Subscribe<ScreenHideSignal>(this.OnCloseScreen);
            this._signalBus.Subscribe<ManualInitScreenSignal>(this.OnManualInitScreen);
            this._signalBus.Subscribe<ScreenSelfDestroyedSignal>(this.OnDestroyScreen);
            this._signalBus.Subscribe<PopupBlurBgShowedSignal>(this.OnPopupBlurBgShowed);
        }

        public void Dispose()
        {
            this._signalBus.Unsubscribe<StartLoadingNewSceneSignal>(this.CleanUpAllScreen);
            this._signalBus.Unsubscribe<ScreenShowSignal>(this.OnShowScreen);
            this._signalBus.Unsubscribe<ScreenHideSignal>(this.OnCloseScreen);
            this._signalBus.Unsubscribe<ManualInitScreenSignal>(this.OnManualInitScreen);
            this._signalBus.Unsubscribe<ScreenSelfDestroyedSignal>(this.OnDestroyScreen);
            this._signalBus.Unsubscribe<PopupBlurBgShowedSignal>(this.OnPopupBlurBgShowed);
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                this.OnEscape();
            }
        }
        
        private void OnEscape()
        {
            if (this._activeScreens.Count > 1)
            {
                if (this._currentActiveScreen != null && this._currentActiveScreen.EscapeAction())
                {
                    this._currentActiveScreen.CloseView();
                }
            }
            else if (this._activeScreens.Count == 1)
            {
                bool valid = this._currentActiveScreen != null && this._currentActiveScreen.EscapeAction();
                if (valid && SceneManager.GetActiveScene().name == SceneName.Main)
                {
                    //TODO open exit
                }
            }
        }

        #region Implement IScreenManager

        public async UniTask<T> OpenScreen<T>() where T : IScreenPresenter
        {
            var nextScreen = await this.GetScreen<T>();
            if (nextScreen != null)
            {
                nextScreen.OpenView();
                return nextScreen;
            }
            else
            {
                Debug.LogError($"The {typeof(T).Name} screen does not exist");
                // Need to implement lazy initialization by Load from resource
                return default;
            }
        }
        public async UniTask<TPresenter> OpenScreen<TPresenter, TModel>(TModel model) where TPresenter : IScreenPresenter<TModel>
        {
            var nextScreen = (await this.GetScreen<TPresenter>());
            if (nextScreen != null)
            {
                nextScreen.OpenView(model);
                return nextScreen;
            }
            else
            {
                Debug.LogError($"The {typeof(TPresenter).Name} screen does not exist");
                // Need to implement lazy initialization by Load from resource
                return default;
            }
        }

        public async UniTask<T> GetScreen<T>() where T : IScreenPresenter
        {
            var screenType = typeof(T);
            if (this._screenPool.TryGetValue(screenType, out var screenController)) return (T)screenController;
            screenController = this.Instantiator.Instantiate<T>();
            await this.InstantiateView(screenController);
            this._screenPool.Add(screenType, screenController);
            return (T)screenController;
        }

        private async UniTask<IScreenView> InstantiateView(IScreenPresenter presenter)
        {
            var screenInfo = presenter.GetCustomAttribute<ScreenInfoAttribute>();
            var viewObject = Instantiate(await GameAssets.LoadAssetAsync(screenInfo.ScreenAssetPath) as GameObject, this.CurrentRootScreen).GetComponent<IScreenView>();
            presenter.SetView(viewObject);
            return viewObject;
        }

        public void CloseCurrentScreen()
        {
            if (this._activeScreens.Count > 0) this._activeScreens.Last().CloseView();
        }

        public void CloseAllScreen()
        {
            var cacheActiveScreens = this._activeScreens.ToList();
            this._activeScreens.Clear();
            foreach (var screen in cacheActiveScreens)
            {
                screen.CloseView();
            }

            this._currentActiveScreen  = null;
            this._previousActiveScreen = null;
        }

        public void CleanUpAllScreen()
        {
            this._activeScreens.Clear();
            this._currentActiveScreen  = null;
            this._previousActiveScreen = null;
            foreach (var screen in this._screenPool)
            {
                screen.Value.Dispose();
            }

            this._screenPool.Clear();
        }
        
        public Transform        CurrentRootScreen { get; set; }
        public Transform        CurrentHiddenRoot { get; set; }
        public IInstantiator    Instantiator      { get; set; }
        public IScreenPresenter CurrentScreen     => this._currentActiveScreen;

        #endregion

        #region Handle events

        private void OnShowScreen(ScreenShowSignal signal)
        {
            this._previousActiveScreen = this._currentActiveScreen;
            this._currentActiveScreen  = signal.ScreenPresenter;
            this._currentActiveScreen.SetViewParent(this.CurrentRootScreen);
            
            // if show the screen that already in the active screens list, remove current one in list and add it to the last of list
            if (this._activeScreens.Contains(signal.ScreenPresenter))
                this._activeScreens.Remove(signal.ScreenPresenter);
            this._activeScreens.Add(signal.ScreenPresenter);

            if (this._previousActiveScreen != null && this._previousActiveScreen != this._currentActiveScreen)
            {
                if (this._currentActiveScreen.IsClosePrevious)
                {
                    this._previousActiveScreen.CloseView();
                    this._previousActiveScreen = null;
                }
                else
                {
                    this._previousActiveScreen.OnOverlap();
                    //With the current screen is screen, the previous screen will be hide after the blur background is shown
                    if (!this._currentActiveScreen.GetType().IsSubclassOfRawGeneric(typeof(BasePopupPresenter<>)))
                        this._previousActiveScreen.HideView();
                }
            }
        }

        private void OnCloseScreen(ScreenHideSignal signal)
        {
            var closeScreenPresenter = signal.ScreenPresenter;
            if (this._activeScreens.LastOrDefault() == closeScreenPresenter)
            {
                // If close the screen on the top, will be open again the behind screen if available
                this._activeScreens.Remove(closeScreenPresenter);
                if (this._activeScreens.Count > 0)
                {
                    var nextScreen = this._activeScreens.Last();
                    if (nextScreen.ScreenStatus == ScreenStatus.Opened)
                        this.OnShowScreen(new ScreenShowSignal() { ScreenPresenter = nextScreen });
                    else
                        nextScreen.OpenView();
                }
            }
            else
            {
                this._activeScreens.Remove(closeScreenPresenter);
            }
            
            closeScreenPresenter?.SetViewParent(this.CurrentHiddenRoot);
        }

        private void OnManualInitScreen(ManualInitScreenSignal signal)
        {
            var screenPresenter = signal.ScreenPresenter;
            var screenType      = screenPresenter.GetType();
            if (this._screenPool.ContainsKey(screenType)) return;
            this._screenPool.Add(screenType, screenPresenter);
            var screenInfo = screenPresenter.GetCustomAttribute<ScreenInfoAttribute>();

            var viewObj = this.CurrentRootScreen.Find(screenInfo.ScreenAssetPath);
            if (viewObj != null)
            {
                screenPresenter.SetView(viewObj.GetComponent<IScreenView>());
            }
            else
                Debug.LogError($"The {screenInfo.ScreenAssetPath} object may be not instantiated in the RootUICanvas!!!");
        }

        private void OnDestroyScreen(ScreenSelfDestroyedSignal signal)
        {
            var screenPresenter = signal.ScreenPresenter;
            var screenType      = screenPresenter.GetType();
            if (this._previousActiveScreen != null && this._previousActiveScreen.Equals(screenPresenter)) this._previousActiveScreen = null;
            this._screenPool.Remove(screenType);
            this._activeScreens.Remove(screenPresenter);
        }

        private void OnPopupBlurBgShowed()
        {
            if (this._previousActiveScreen != null && this._previousActiveScreen.ScreenStatus != ScreenStatus.Hide && this._previousActiveScreen != this._currentActiveScreen)
            {
                // Only hide if the previous is popup
                if (this._previousActiveScreen.GetType().IsSubclassOfRawGeneric(typeof(BasePopupPresenter<>)))
                    this._previousActiveScreen.HideView();
            }
        }

        #endregion
    }
}