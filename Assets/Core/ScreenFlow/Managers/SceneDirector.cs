namespace Core.ScreenFlow.Managers
{
    using Core.LocalDataServices;
    using Core.ScreenFlow.Signals;
    using Cysharp.Threading.Tasks;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using Zenject;

    public static class SceneName
    {
        public const string Loading = "LoadingScene";
        public const string Main    = "MainScene";
        public const string Battle  = "BattleScene";
    }

    /// <summary>Load, unload scenes are wrapped here </summary>
    public class SceneDirector
    {
        private readonly SignalBus          signalBus;
        private readonly ILocalDataServices _localDataServices;
        public static    string             CurrentSceneName = SceneName.Loading;
        public SceneDirector(SignalBus signalBus, ILocalDataServices localDataServices)
        {
            this.signalBus               = signalBus;
            this._localDataServices = localDataServices;
        }

        /// <summary>Load scene async by name </summary>
        public async UniTask LoadSingleSceneAsync(string sceneName)
        {
            this.signalBus.Fire<StartLoadingNewSceneSignal>();
            var lastScene = CurrentSceneName;
            CurrentSceneName = sceneName;
            await SceneManager.LoadSceneAsync(sceneName);
            Resources.UnloadUnusedAssets();
            
            //save local data before finish loading new scene
            this._localDataServices.StoreAllToLocal();
            
            this.signalBus.Fire<FinishLoadingNewSceneSignal>();
        }

        /// <summary>Load scene in build by name </summary>
        public async UniTask LoadSingleSceneInBuildAsync(string sceneName)
        {
            this.signalBus.Fire<StartLoadingNewSceneSignal>();
            var lastScene = CurrentSceneName;
            CurrentSceneName = sceneName;
            await SceneManager.LoadSceneAsync(sceneName);
            Resources.UnloadUnusedAssets();
            
            //save local data before finish loading new scene
            this._localDataServices.StoreAllToLocal();
            
            this.signalBus.Fire<FinishLoadingNewSceneSignal>();
        }

        /// <summary>Unload scene async by name </summary>
        public async UniTask UnloadSceneAsync(string sceneName)
        {
            await SceneManager.UnloadSceneAsync(sceneName);
            Resources.UnloadUnusedAssets();
        }

        #region Shortcuts

        public async void LoadLoadingScene() => await this.LoadSingleSceneInBuildAsync(SceneName.Loading);
        public async void LoadMainScene()    => await this.LoadSingleSceneAsync(SceneName.Main);
        public async void LoadBattleScene()  => await this.LoadSingleSceneAsync(SceneName.Battle);

        #endregion
    }
}