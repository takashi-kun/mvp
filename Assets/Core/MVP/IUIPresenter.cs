namespace Core.MVP
{
    /// <summary>
    /// Represent logic affect to views
    /// </summary>
    public interface IUIPresenter
    {
        public void SetView(IUIView viewInstance);
    }

    public interface IUIPresenterWithModel<TModel> : IUIPresenter
    {
        public void SetView(IUIView viewInstance, TModel model);
    }
}