namespace Core.MVP
{
    /// <summary>
    /// Represent view, contains render logic.
    /// May contain input data stream (onClick, onSwipe...)
    /// </summary>
    public interface IUIView
    {
        
    }
}