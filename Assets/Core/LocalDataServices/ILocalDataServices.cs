namespace Core.LocalDataServices
{
    public interface ILocalDataServices
    {
        /// <summary>
        /// Save a class data to local
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data">class data</param>
        /// <param name="force"> if true, save data immediately to local</param>
        /// <typeparam name="T"> type of class</typeparam>
        void Save<T>(string key, T data, bool force = false) where T : class;
        
        /// <summary>
        /// Save a class data to local with default key = "LD-typeof(T).Name"
        /// </summary>
        void Save<T>(T data, bool force = false) where T : class;
        
        /// <summary>
        /// Load data from local
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Load<T>(string key) where T : class, new();
        
        /// <summary>
        /// Load data from local with default key = "LD-typeof(T).Name"
        /// </summary>
        T Load<T>() where T : class, new();
        
        public void StoreAllToLocal();
    }
}