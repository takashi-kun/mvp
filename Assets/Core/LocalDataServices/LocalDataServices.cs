namespace Core.LocalDataServices
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using UnityEngine;

    public class LocalDataServices : ILocalDataServices
    {
        private const string LocalDataPrefix = "LD-";

        private Dictionary<string, object> localDataCaches = new Dictionary<string, object>();


        /// <summary>
        /// Save a class data to local
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data">class data</param>
        /// <param name="force"> if true, save data immediately to local</param>
        /// <typeparam name="T"> type of class</typeparam>
        public void Save<T>(string key, T data, bool force = false) where T : class
        {
            if (!this.localDataCaches.ContainsKey(key))
            {
                this.localDataCaches.Add(key, data);
            }

            if (force)
            {
                var json = JsonConvert.SerializeObject(data);
                PlayerPrefs.SetString(key, json);
                Debug.Log("Save " + key + ": " + json);
                PlayerPrefs.Save();
            }
        }


        /// <summary>
        /// Save a class data to local with default key = "LD-typeof(T).Name"
        /// </summary>
        public void Save<T>(T data, bool force = false) where T : class { this.Save<T>(LocalDataPrefix + typeof(T).Name, data, force); }

        /// <summary>
        /// Load data from local
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Load<T>(string key) where T : class, new()
        {
            if (this.localDataCaches.TryGetValue(key, out var cache))
            {
                return (T)cache;
            }

            var json   = PlayerPrefs.GetString(key);
            var result = string.IsNullOrEmpty(json) ? new T() : JsonConvert.DeserializeObject<T>(json);
            this.localDataCaches.Add(key, result);
            return result;
        }

        /// <summary>
        /// Load data from local with default key = "LD-typeof(T).Name"
        /// </summary>
        public T Load<T>() where T : class, new() { return this.Load<T>(LocalDataPrefix + typeof(T).Name); }

        public void StoreAllToLocal()
        {
            foreach (var localData in this.localDataCaches)
            {
                PlayerPrefs.SetString(localData.Key, JsonConvert.SerializeObject(localData.Value));
            }

            PlayerPrefs.Save();
            Debug.Log("Save Data To File");
        }
    }
}